import React from 'react';
import { observable, action, computed } from "mobx";

import { ITodo, IStorage } from './interfaces';

// Getting a list tasks from local storage browser
const myLocStorage = localStorage.getItem( "todoList" );

export class TodoStore {
  @observable todoInput = React.createRef<HTMLInputElement>();
  @observable todos: ITodo[] = [];
  @observable todoFilter: string = 'all';
  @observable allChecked: boolean = false;
  @observable beforeEditCache: string = '';
  
  myStorage: IStorage = {
    storage: []
  }

// Write the tasks from local storage browser in the todos
  viewTodos = () => {
    if( myLocStorage !== null ) {
      this.todos = [...( JSON.parse( myLocStorage ).storage )];
    }
  }

// Task add 
  @action
  addTodo = ( event: { key: string; } ) => {
    if ( event.key === 'Enter' ) {
      const todoInput = this.todoInput.current!.value;

      if( todoInput.trim().length === 0 ) {
          return;
      }

      this.todos.push({
        id: Date.now(), 
        task: todoInput, 
        isCompleted: false, 
        editing: false,
        created: Date.now(),
        edited: null,
        completed: null
      })

      this.todoInput.current!.value = '';

      this.localStorageCompleted();
    }
  }

// Task delete
  @action
  removeTodo = ( index: number ): void => {
    this.todos = [...this.todos.filter( todo => todo.id !== index )];
    this.localStorageCompleted();
  }

// Task complete
  @action
  completeTodo = ( index: number ): void => {
    this.todos.find( todo => todo.id === index )!.isCompleted = 
                      !this.todos.find( todo => todo.id === index )!.isCompleted;
    if ( this.todos.find( todo => todo.id === index )!.isCompleted ) {
      this.todos.find( todo => todo.id === index )!.completed = Date.now();
    }else{
      this.todos.find( todo => todo.id === index )!.completed = null;
    }
    this.localStorageCompleted();
  }

// Activate button All | Active | Completed 
  @action
  updateFilter = ( filter: string ): void => {
    this.todoFilter = filter;
  }

// Select all todos completed
  @action
  checkAllTodos = ( event: { target: { checked: boolean; }; } ): void => {
    this.allChecked = event.target.checked;

    if ( this.allChecked ) {
      this.todos.forEach( todo => { 
        if( todo.isCompleted === false )
        todo.isCompleted = !todo.isCompleted  
      })
    } else {
      this.todos.forEach( todo =>  
        todo.isCompleted = !todo.isCompleted  
      )  
    }
    this.localStorageCompleted();
  }

// Deletes all completed tasks
  @action
  clearCompleted = (): void => {
    this.todos = [...this.todos.filter( todo => !todo.isCompleted )];
    this.localStorageCompleted();
  } 

// Edit task
  @action 
  editTodo = ( todo: ITodo ) => {
    todo.editing = true;
    this.beforeEditCache = todo.task;

    const index = this.todos.findIndex( item => item.id === todo.id );

    this.todos.splice( index, 1, todo );
  }

// If the input-edit is empty task = cache else task = value
  @action 
  doneEdit = ( todo: ITodo, event: { target: { value: string; }; } ) => {
    todo.editing = false;

    if ( event.target.value.trim().length === 0 ) {
      todo.task = this.beforeEditCache;
    } else {
      todo.task = event.target.value;
      this.localStorageCompleted();
    }
  }

// Cancel edit task
  @action 
  cancelEdit = ( todo: ITodo ) => {
    todo.task = this.beforeEditCache;
    todo.editing = false;

    const index = this.todos.findIndex( item => item.id === todo.id );

    this.todos.splice( index, 1, todo );
  }

// Create date time edited
  @action
  editedDateTime = ( todo: ITodo ) => {
    todo.edited = Date.now();
  }

// Get active tasks | completed tasks
  @computed
  get todosFiltered (): ITodo[] {
    if ( this.todoFilter === 'active' ) {
      return this.todos.filter( todo => todo.isCompleted === false );
    } else if ( this.todoFilter === 'completed' ) {
      return this.todos.filter( todo => todo.isCompleted === true );
    }
    return this.todos;
  }

// Write the todos in local storage browser
  localStorageCompleted = (): void => {
    this.myStorage.storage = [...this.todos];
    localStorage.setItem( "todoList", JSON.stringify( this.myStorage ) );
  }

}

export const store = new TodoStore();
