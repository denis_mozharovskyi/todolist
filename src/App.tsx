import React, { Component } from 'react';

import TodoTitle from './components/TodoTitle';
import TodoForm from './components/TodoForm';
import TodoList from './components/TodoList';
import Footer from './components/Footer';

import './App.css';

import { inject, observer } from 'mobx-react'; 
import { ITodoStore } from './interfaces';

interface AppProps {
  store?: ITodoStore
}

@inject('store') 
@observer 
class App extends Component<AppProps> { 
  render() {
    return (
      <div className="App">
        <TodoTitle />
        <TodoForm  />
        <TodoList  /> 
        <Footer    />
      </div>
    );
  }    
  componentDidMount() {
    this.props.store!.viewTodos();
  }
}

export default App;
