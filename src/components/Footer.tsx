import React from 'react';

import { inject, observer } from 'mobx-react';

import { ITodo } from '../interfaces';

// Calculates the number of active tasks
const itemsLeftActive = ( todos: ITodo[] ): number => {
  if ( todos.length === 0 ) {
    return 0;
  }
  return todos.filter ( todo => !todo.isCompleted ).length;
};

const Footer = inject( 'store' )( observer( ({ store }) => {
  return (
// footer active when the tasks > 0
    <footer hidden={ store.todos.length > 0 ? false : true }>
  {/* Field checkbox and info the tasks */}
      <div className="extra-container">
        <div>
    {/* Checkbox all todos completed */}
          <label    className="checkall-label">
            <input  className="checkall-label-input" 
                    type="checkbox" 
                    checked={ store.allChecked } 
                    onChange={ ( event ) => store.checkAllTodos( event ) } 
            />
            Check all
          </label>
        </div>
    {/* End checkbox */}
    {/* Number of active and percentage completed tasks */}
        <div> { itemsLeftActive( store.todos ) } items left / percent complete: 
              { Math.round( store.todos.filter( ( todo: ITodo ) => todo.isCompleted ).length 
                * 100/ store.todos.length ) } %
        </div>
      </div>
    {/* End info */}
  {/* End field */}

  {/* Group buttons All, Active, Completed and Clear Completed */}
      <div className="extra-container">
        <div>
    {/* Shows all the tasks */}
          <button 
            className= { store.todoFilter === 'all' 
                        ? "btn  btn-outline-secondary btn-sm active button-update-filter" 
                        : "btn btn-outline-secondary btn-sm button-update-filter" } 
            title="View all tasks"
            onClick={ () => store.updateFilter( 'all' ) }
          >
            All
          </button>
    {/* Shows all active the tasks */}         
          <button 
            className={ store.todoFilter === 'active' 
                        ? "btn btn-outline-secondary btn-sm active button-update-filter" 
                        : "btn btn-outline-secondary btn-sm button-update-filter" }
            title="View active tasks"
            onClick={ () => store.updateFilter( 'active' ) }
          >
            Active
          </button>
    {/* Shows all completed the tasks */}          
          <button 
            className={ store.todoFilter === 'completed' 
                        ? "btn btn-outline-secondary btn-sm active button-update-filter" 
                        : "btn btn-outline-secondary btn-sm button-update-filter" } 
            title="View completed tasks"
            onClick={ () => store.updateFilter( 'completed' ) }
          >
            Completed
          </button>
        </div>
        
        <div>
    {/* Deletes all completed tasks */}  
          <button className="btn btn-outline-info btn-sm" 
                  title="Clear all completed tasks"
                  onClick={ store.clearCompleted }>
            Clear Completed
          </button>
        </div>
      
      </div>
  {/* End group buttons */}     
    </footer>
// End footer 
  );
}));

export default Footer;
