import React from 'react';

import { inject, observer } from 'mobx-react';

import TodoTask from './TodoTask';

import { ITodo } from '../interfaces';


const TodoList = inject('store')(observer(({ store }) => {
  return (
    <ul className="list-group">
      {store.todosFiltered.map((todo: ITodo) => (  
        <TodoTask 
          key={todo.id}
          todo={todo}
        />
      ))}
    </ul>
  );
}));

export default TodoList;
