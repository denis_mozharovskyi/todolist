import React from 'react';

import { TODOTITLE } from '../constants';


const TodoTitle = () => {
  return (
    <h1 className="App-header">{TODOTITLE}</h1>
  );  
}

export default TodoTitle;
