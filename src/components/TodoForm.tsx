import React from 'react';

import { inject, observer } from 'mobx-react';


const TodoForm = inject('store')(observer(({ store }) => {    
  return (
      <input 
        type="text"
        className="form-control" 
        placeholder="What needs to be done" 
        onKeyUp={store.addTodo}
        ref={store.todoInput}
        title="Fill in this field."
        required
      />
  );
}));

export default TodoForm;
