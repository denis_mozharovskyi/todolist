import React from 'react';

import { inject, observer } from 'mobx-react';

const TodoTask = inject( 'store' )( observer( ({ todo, store }) => {
  return (
// Swow task
    <li         key={ todo.id } 
                className="todo-item"
  // Show task title created & edited & completed
                title={ 
                  ( "Create task : " + new Date( todo.created ).toLocaleString() ) 
                  + 
                  ((todo.edited) ? "\nEdit task: " + 
                                    new Date( todo.edited ).toLocaleString() :
                                      '')
                  + 
                  ((todo.completed) ? "\nComplete task: " + 
                                    new Date( todo.completed ).toLocaleString() :
                                      '')  
                }>
      <div      className="todo-item-left">
  {/* Checkbox completed = true | false */}
        <input  type="checkbox"
                title="Complete task." 
                checked={ todo.isCompleted ? true : false } 
                onChange={ () => store.completeTodo( todo.id ) } 
        />
  {/* Task content */}
        { !todo.editing && 
            <div  className={ todo.isCompleted ? 
                                    'completed todo-item-label' : 
                                    'todo-item-label' }
                  title="Double click for edit."
                  onDoubleClick={ ()=> store.editTodo( todo ) }>
              { todo.task }
            </div>
        }
  {/* Task edit input */}
        { todo.editing &&
            <input  
                  className="form-control"
                  type="text" autoFocus
                  defaultValue={ todo.task }
                  onBlur={ ( event ) => store.doneEdit( todo, event ) }
                  onKeyUp={ ( event ) => {
                              if ( event.key === 'Enter' ) {
                                store.doneEdit( todo, event );
                                store.editedDateTime( todo );
                              } else if ( event.key === 'Escape' ) {
                                store.cancelEdit( todo );
                              }
                            } 
                  }     
          />
        }      
      </div>
  {/* Task delete */}
      <span     className="remove-task"    
                title="Delete Task"
                onClick={ () => store.removeTodo( todo.id ) }>
        &#88;
      </span>  
    </li>
  );
}));

export default TodoTask;
