export interface ITodo  {
  id: number;
  task: string;
  isCompleted: boolean;
  editing: boolean;
  created: number;
  edited: number | null;
  completed: number | null;
}

export interface IStorage {
  storage: ITodo[];
}

export interface ITodoStore {
  todoInput: HTMLInputElement;
  todos: ITodo[];
  todoFilter: string;
  allChecked: boolean;
  beforeEditCache: string;
  myStorage: IStorage;
  todosFiltered: ITodo[];
  viewTodos(): void;
  addTodo(e:{key:string;}): void;
  removeTodo(index: number): void;
  completeTodo(index: number): void;
  updateFilter(filter: string): void;
  checkAllTodos(event: { target: { checked: boolean; };}): void;
  clearCompleted(): void;
  localStorageCompleted(): void;
}
